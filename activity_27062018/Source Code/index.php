<!DOCTYPE html>
<html>
	<title>Inventory</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	
	<body>
		<br><br>
		<div class="container">
			<a href="index.php"><button class="button button2">Product</button></a>
			<a href="category.php"><button class="button button2">Category</button></a>
  			<h2>Product</h2>
  			<table class="table table-hover">
				<thead>
			      <tr>
			        <th>Product ID</th>
			        <th>Category</th>
			        <th>Name</th>
			        <th>Desc</th>
			        <th>Desc</th>
			        <th></th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			      </tr>
			      <tr>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			      </tr>
			      <tr>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			      </tr>
			    </tbody>
			  </table>
			  <a href="addProduct.php"><button class="button button1">Add</button></a>
		</div>
	</body>
</html>